# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in G:\Android Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes Signature
-keepattributes Exceptions
#Retrofit
-dontwarn retrofit2.**
-keep class retrofit2.** { *; }
#okhttp3

-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.*

#gson models
-keep class com.devchimps.partyapp.model.** { *; }
-keep class com.devchimps.partyapp.rest.model.** { *; }

#EventBus
-keepclassmembers class ** {
    public void onEvent*(**);
}

#Gradle Retrolambda Plugin
-dontwarn java.lang.invoke.*

#SuperRecyclerView
-dontwarn com.malinskiy.superrecyclerview.SwipeDismissRecyclerViewTouchListener*

#rxjava
-dontwarn rx.internal.util.unsafe.**

#reflection for apply font to toolbar
-keep class android.support.design.widget.CollapsingToolbarLayout { *; }
-keep class android.support.design.widget.CollapsingTextHelper { *; }

#picasso
-dontwarn com.squareup.okhttp.**
