package com.devchimps.partyapp;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.devchimps.partyapp.activity.LoginSocialActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by Matrix on 29/03/2016.
 */
@RunWith(AndroidJUnit4.class)
public class SocialLoginActivityTest {
    @Rule
    public final ActivityTestRule<LoginSocialActivity> loginSocialActivityActivityTestRule = new ActivityTestRule<>(LoginSocialActivity.class);

    @Test
    public void checkIfSocialLoginsButtonAreVisible() {
        onView(withText("Log in with Google")).check(matches(isDisplayed()));
        onView(withText("Log in with Facebook")).check(matches(isDisplayed()));
    }

    @Test
    public void testLoginUsingGoogle() {
        //onView(withId(R.id.btn_sign_in_google)).perform(click()).check(matches(withText("Join a party")));
    }

    @Test
    public void testLoginUsingFacebook() {
        //onView(withId(R.id.btn_sign_in_facebook)).perform(click()).check(matches(withText("Join a party")));
    }

}
