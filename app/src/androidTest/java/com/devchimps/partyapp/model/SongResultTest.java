package com.devchimps.partyapp.model;

import com.devchimps.partyapp.network.model.SongResult;

import org.junit.Test;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;

/**
 * Created by Matrix on 31/03/2016.
 */
public class SongResultTest {

    @Test
    public void hashCodeEquals() {
        EqualsVerifier.forExamples(new SongResult("title1", "artist1", "emptyurl1", "trackid1"), new SongResult("title2", "artist2", "emptyurl2", "trackid2"))
                .suppress(Warning.NULL_FIELDS)
                .verify();
    }
}
