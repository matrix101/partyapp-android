package com.devchimps.partyapp.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.devchimps.partyapp.R;

import butterknife.BindView;

/**
 * Created by Matrix on 06/05/2016.
 */
public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    protected void initToolbar() {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa_regular.ttf");
        toolbarTitle.setTypeface(font);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
    }
}
