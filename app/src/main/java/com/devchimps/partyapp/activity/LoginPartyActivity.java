package com.devchimps.partyapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.PartyAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.network.model.Coordinate;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.network.model.UserPartiesResponse;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.Utils;
import com.google.android.gms.location.LocationRequest;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.PlaceDetails;
import com.tbruyelle.rxpermissions.RxPermissions;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPartyActivity extends BaseActivity {

    private static final String TAG = "LoginPartyActivity";

    @BindView(R.id.rv_parties)
    RecyclerView recyclerViewParties;
    @BindView(R.id.loading_panel)
    RelativeLayout loadingPanel;
    @BindView(R.id.error_panel)
    RelativeLayout errorPanel;
    @BindView(R.id.txt_error_message)
    TextView txtErrorMessage;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.swipe_to_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.places_autocomplete)
    PlacesAutocompleteTextView autocompleteTextView;


    private PartyAppProfile partyAppProfile;
    private PartyAdapter partyAdapter;
    private Location currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        partyAppProfile = Storage.getPartyappProfile(getApplicationContext());

        if (partyAppProfile == null) {
            Log.d(TAG, "PartyAppProfile is null, starting LoginActivity");
            startLoginActivity();
            return;
        }
        if (partyAppProfile.hasParty()) {
            Log.d(TAG, "User has a party, starting MainActivity");
            startMainActivity();
            return;
        }
        crashlyticsLogin();
        setContentView(R.layout.activity_party_login);
        ButterKnife.bind(this);
        initToolbar();
        setupInstances();
        toolbarTitle.setText(R.string.join_a_party);
        showPartiesAroundMe();
    }

    private void crashlyticsLogin() {
        Crashlytics.setUserIdentifier(partyAppProfile.getIdUser());
        Crashlytics.setUserEmail(partyAppProfile.getEmail());
        Crashlytics.setUserName(partyAppProfile.getFullName());
    }


    private void setupInstances() {
        recyclerViewParties.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.white);
        swipeRefreshLayout.setColorSchemeResources(R.color.primary);
        swipeRefreshLayout.setOnRefreshListener(this::showPartiesAroundMe);
    }

    private void showPartiesFromLocation(Coordinate coordinate) {
        App.getRestClient().getApiService().getPartiesNearLocation(coordinate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    hideError();
                    showLoading();
                })
                .subscribe(new Subscriber<UserPartiesResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoading();
                        showError(R.string.no_internet_connection);
                    }

                    @Override
                    public void onNext(UserPartiesResponse userPartiesResponse) {
                        hideLoading();
                        showParties(userPartiesResponse);
                    }
                });
    }

    private void getUserParties() {
        App.getRestClient().getApiService().getUserParties(partyAppProfile.getIdUser())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    hideError();
                    showLoading();
                })
                .subscribe(new Subscriber<UserPartiesResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoading();
                        showError(R.string.no_internet_connection);
                    }

                    @Override
                    public void onNext(UserPartiesResponse userPartiesResponse) {
                        hideLoading();
                        showParties(userPartiesResponse);
                    }
                });

    }

    private void getPartiesByName(String partyName) {
        App.getRestClient().getApiService().getPartyByName(partyName)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    hideError();
                    showLoading();
                })
                .subscribe(new Subscriber<UserPartiesResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoading();
                        showError(R.string.no_internet_connection);
                    }

                    @Override
                    public void onNext(UserPartiesResponse userPartiesResponse) {
                        hideLoading();
                        if (userPartiesResponse == null) {
                            showError(R.string.no_internet_connection);
                        } else if (userPartiesResponse.getMessage().equalsIgnoreCase("success")) {
                            showParties(userPartiesResponse);
                        }
                    }
                });
    }

    //checks if gps is enabled
    //retriving open parties around user
    private void showPartiesAroundMe() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
        RxPermissions.getInstance(this)
                .request(Manifest.permission.ACCESS_FINE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                .setNumUpdates(1);
                        if (Utils.isAirplaneModeOn(this)) {
                            hideLoading();
                            showError(R.string.airplane_mode_is_on);
                        }

                        ReactiveLocationProvider locationProvider = new ReactiveLocationProvider(this);
                        locationProvider.getUpdatedLocation(request)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSubscribe(() -> {

                                })
                                .subscribe(new Subscriber<Location>() {
                                    @Override
                                    public void onCompleted() {
                                        Log.i(TAG, "onCompleted");
                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        Log.e(TAG, "Failed to get position: " + e.getMessage());
                                    }

                                    @Override
                                    public void onNext(Location location) {
                                        Log.i(TAG, "Current position: (" + location.getLatitude() + "," + location.getLongitude() + ")");
                                        currentLocation = location;
                                        Coordinate coordinate = new Coordinate(location.getLatitude(), location.getLongitude());
                                        showPartiesFromLocation(coordinate);
                                    }
                                });
                    } else {
                        //TODO ask for party name
                        Log.i(TAG, "GPS permission denied, showing an error message");
                        buildAlertMessageDeniedPermission();
                    }
                });
    }

    private void buildAlertMessageDeniedPermission() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You denied to access your current position") //todo messages
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    showPartiesAroundMe();
                })
                .setNegativeButton("No", (dialog, id) -> {
                    dialog.cancel();
                    //TODO what to do?
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                })
                .setNegativeButton("No", (dialog, id) -> {
                    dialog.cancel();
                    //TODO what to do?
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_party, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                logoutFromPartyapp();
                break;
            case R.id.action_search:
                showSearchBox();
                searchPartiesNearLocation();
                break;
            case R.id.action_profile:
                startProfileActivity();
                break;
            case R.id.action_setting:
                //showSetting();
                break;
            default:
                break;
        }
        return true;
    }

    private void showSearchBox() {
        toolbarTitle.setVisibility(View.GONE);
        swipeRefreshLayout.setEnabled(false);
        if (currentLocation != null)
            autocompleteTextView.setCurrentLocation(currentLocation);
    }

    private void searchPartiesNearLocation() {
        autocompleteTextView.setOnPlaceSelectedListener(place -> autocompleteTextView.getDetailsFor(place, new DetailsCallback() {
            @Override
            public void onSuccess(PlaceDetails placeDetails) {
                Log.d(TAG, placeDetails.toString());
                Coordinate coordinate = new Coordinate(placeDetails.geometry.location.lat, placeDetails.geometry.location.lng);
                showPartiesFromLocation(coordinate);
            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.e(TAG, "something went wrong while retriving coordinates");
            }
        }));
        autocompleteTextView.setVisibility(View.VISIBLE);
    }

    private void logoutFromPartyapp() {
        Storage.deletePartyAppProfile(this.getApplicationContext());
        startLoginActivity();
    }

    private void startProfileActivity() {
        Intent startProfileActivityIntent = new Intent(this, ProfileActivity.class);
        Log.i(TAG, "Starting Profile activity for user: " + partyAppProfile.getIdUser());
        startProfileActivityIntent.putExtra("profile_id", partyAppProfile.getIdUser());
        startActivity(startProfileActivityIntent);
    }

    private void startMainActivity() {
        Intent startMainActivityIntent = new Intent(this, MainActivity.class);
        startActivity(startMainActivityIntent);
        finish();
    }

    private void startLoginActivity() {
        Intent intent = new Intent(getApplicationContext(), LoginSocialActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLoading() {
        recyclerViewParties.setVisibility(View.GONE);
        loadingPanel.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
        loadingPanel.setVisibility(View.GONE);
        recyclerViewParties.setVisibility(View.VISIBLE);

    }

    private void showError(int stringResource) {
        Log.w(TAG, "No internet connection");
        recyclerViewParties.setVisibility(View.GONE);
        loadingPanel.setVisibility(View.GONE);
        txtErrorMessage.setText(stringResource);
        errorPanel.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        errorPanel.setVisibility(View.GONE);
    }

    private void showParties(UserPartiesResponse userPartiesResponse) {
        if (userPartiesResponse.getMessage().equalsIgnoreCase("success")) {
            partyAdapter = new PartyAdapter(userPartiesResponse.getUserParties(), getApplicationContext());
            recyclerViewParties.setAdapter(partyAdapter);
            if (userPartiesResponse.getUserParties().size() == 0) {
                txtErrorMessage.setText(R.string.no_parties);
                errorPanel.setVisibility(View.VISIBLE);
            }
        }
    }
}