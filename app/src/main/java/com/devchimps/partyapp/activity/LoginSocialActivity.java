package com.devchimps.partyapp.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.SocialViewPagerAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.network.model.LoginResponse;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.network.model.SocialProfile;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.SnackbarFactory;
import com.devchimps.partyapp.util.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.tbruyelle.rxpermissions.RxPermissions;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.relex.circleindicator.CircleIndicator;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@SuppressWarnings("deprecation")
public class LoginSocialActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 0;
    private static final String TAG = "LoginSocialActivity";
    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.circle_indicator)
    CircleIndicator indicator;
    @BindView(R.id.btn_sign_in_google)
    Button btnSignInGoogle;
    @BindView(R.id.btn_sign_in_facebook)
    Button btnSignInFacebook;
    LoginButton loginButton;
    @BindView(R.id.rootLayout)
    LinearLayout rootLayout;
    @BindView(R.id.loadingPanel)
    RelativeLayout loadingPanel;

    private GoogleApiClient mGoogleApiClient;

    private CallbackManager callbackManager;

    private SocialProfile profile;

    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        PartyAppProfile partyappProfile = Storage.getPartyappProfile(context);

        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_social_login);
        ButterKnife.bind(this);

        initToolbar();

        setupFacebookLogin();
        setupGoogleLogin();

        //ViewPager
        SocialViewPagerAdapter socialViewPagerAdapter = new SocialViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(socialViewPagerAdapter);
        indicator.setViewPager(viewPager);

        if (partyappProfile != null)
            startJoinPartyActivity();
        else {
            signOutFromGplus();
            signOutFromFacebook();
        }
    }

    private void setupGoogleLogin() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    private void setupFacebookLogin() {
        loginButton = new LoginButton(this);
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.d(TAG, response.getRawResponse());
                            SocialProfile profile = getFacebookProfileInfo(response);
                            loginUserInPartyapp(profile);
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "name,picture,email");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "onCancel clicked");
            }

            @Override
            public void onError(FacebookException e) {
                Log.e(TAG, "Error on facebook login " + e.getMessage());
                SnackbarFactory.make(rootLayout, getString(R.string.social_login_facebook_error), R.color.primary, Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void loginUserInPartyapp(SocialProfile socialProfile) {
        loadingPanel.setVisibility(View.VISIBLE);

        //FIXME this method is called in both cases, google login and facebook login but we have 2 different api for login? why?
        App.getRestClient().getApiService().loginFacebook(socialProfile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        loadingPanel.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "Something went wrong while logging in: " + e.getMessage());
                        loadingPanel.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        if (loginResponse != null) {
                            Storage.savePartyAppProfile(context, loginResponse.getPartyAppProfile());
                            startJoinPartyActivity();
                        } else showNoNetworkSnackBar();
                    }
                });
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @OnClick(R.id.btn_sign_in_google)
    protected void onClickGoogleSignIn() {
        Log.d(TAG, "Google Login button clicked");
        if (Utils.isNetworkOn(getApplicationContext())) {
            mGoogleApiClient.connect();
        } else {
            //TODO use a dialog for show no internet connection?
            showNoNetworkSnackBar();
        }
    }

    @OnClick(R.id.btn_sign_in_facebook)
    protected void onClickFacebookSignIn() {
        Log.d(TAG, "Facebook Login button clicked");
        if (Utils.isNetworkOn(getApplicationContext())) {
            loginButton.performClick();
        } else {
            //TODO use a dialog for show no internet connection?
            showNoNetworkSnackBar();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        if(connectionResult.getErrorCode()== ConnectionResult.SERVICE_MISSING) {
            SnackbarFactory.make(viewPager, "Google Play services is missing", R.color.primary, Snackbar.LENGTH_LONG).show();
            return;
        }
        if(connectionResult.getErrorCode()== ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED) {
            SnackbarFactory.make(viewPager, "Google Play services update required", R.color.primary, Snackbar.LENGTH_LONG).show();
            return;
        }
        if (connectionResult.hasResolution()) {
            try {
                connectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Could not resolve ConnectionResult.", e);
                mGoogleApiClient.connect();
            }
        } else {
            showNoNetworkSnackBar();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);

        if (requestCode == RC_SIGN_IN) {
            // If the error resolution was not successful we should not resolve further.
            mGoogleApiClient.connect();
        }
        Log.d("Facebook activityResult", "requestCode: " + requestCode + " responseCode:");
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }

    @SuppressLint("RxSubscribeOnError")
    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.

        RxPermissions.getInstance(this)
                .request(Manifest.permission.GET_ACCOUNTS)
                .subscribe(granted -> {
                    if (granted) {
                        Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                        String personName = currentPerson.getDisplayName();
                        String personPhotoUrl = currentPerson.getImage().getUrl();
                        String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                        profile = new SocialProfile(personName, personPhotoUrl, email);
                        Log.i("GoogleProfileInfo", "Name: " + personName + ", email: " + email + ", Image: " + personPhotoUrl);
                        loginUserInPartyapp(profile);
                    } else {
                        Log.d(TAG, "User denied google permissions");
                        //fixme ask again?
                    }
                });
    }


    private void startJoinPartyActivity() {
        Intent intent = new Intent(this, LoginPartyActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    private SocialProfile getFacebookProfileInfo(GraphResponse response) {
        String displayName = "", imageUrl = "", email = "", id;
        try {
            displayName = response.getJSONObject().getString("name");
            id = response.getJSONObject().getString("id");
            imageUrl = "http://graph.facebook.com/" + id + "/picture?type=large";
            email = response.getJSONObject().getString("email");
            return new SocialProfile(displayName, imageUrl, email);
        } catch (JSONException e) {
            Log.e(TAG, "Error while parsing facebook user");
            return new SocialProfile(displayName, imageUrl, email);
        }
    }

    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }
    }

    private void signOutFromFacebook() {
        LoginManager.getInstance().logOut();
    }

    private void revokeGplusAccess() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(arg0 -> {
                        Log.e(TAG, "User access revoked!");
                        mGoogleApiClient.connect();
                    });
        }
    }

    private void showNoNetworkSnackBar() {
        SnackbarFactory.make(rootLayout, R.string.snackbar_no_internet_connection, R.color.primary, Snackbar.LENGTH_LONG).show();
    }
}