package com.devchimps.partyapp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.PlayListAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.model.Playlist;
import com.devchimps.partyapp.model.Song;
import com.devchimps.partyapp.network.ItemTypeAdapterFactory;
import com.devchimps.partyapp.network.PartyappSocket;
import com.devchimps.partyapp.network.model.InsertSongResponse;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.network.model.SongResult;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.SnackbarFactory;
import com.devchimps.partyapp.util.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import hugo.weaving.DebugLog;
import io.fabric.sdk.android.Fabric;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Francesco on 20/06/2015.
 */
public class MainActivity extends AppCompatActivity {

    private static final int GET_SONG_REQUEST = 1;

    private static final String TAG = MainActivity.class.getName();

    @BindView(R.id.rv_main_songs)
    SuperRecyclerView recyclerView;
    @BindView(R.id.txt_empty_message)
    TextView txtEmptyMessage;
    @BindView(R.id.collapsingToolbarLayout)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fabBtn)
    FloatingActionButton fabBtn;
    @BindView(R.id.img_song)
    ImageView imgSong;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_artist)
    TextView txtArtist;
    @BindView(R.id.img_user)
    CircleImageView imgUser;
    @BindView(R.id.img_artist)
    ImageView imgArtist;

    private PartyAppProfile partyAppProfile;
    private PartyappSocket socket;

    private Song currentTrack;
    private Playlist playlist;
    private PlayListAdapter playListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        partyAppProfile = Storage.getPartyappProfile(getApplicationContext());
        if (partyAppProfile != null && !partyAppProfile.hasParty()) {
            startPartyLoginActivity();
        }
        connectSocket();
        setupSocket();

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initToolbar();
        initInstances();
    }

    private void initInstances() {
        txtEmptyMessage.setText(R.string.no_songs_in_playlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        playlist = new Playlist();
        playListAdapter = new PlayListAdapter(playlist.getPlaylist(), getApplicationContext(), partyAppProfile.getIdUser());
    }

    @OnClick(R.id.fabBtn)
    public void fabClick() {
        startSearchSongActivity();
    }

    private void initToolbar() {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa_regular.ttf");
        setSupportActionBar(toolbar);
        collapsingToolbarLayout.setTitle("partyapp");
        collapsingToolbarLayout.setExpandedTitleColor(ContextCompat.getColor(getApplicationContext(),android.R.color.transparent));
        collapsingToolbarLayout.setCollapsedTitleTypeface(font);
        collapsingToolbarLayout.setExpandedTitleTypeface(font);
    }

    private void startPartyLoginActivity() {
        Intent intent = new Intent(this, LoginPartyActivity.class);
        startActivity(intent);
        finish();
    }

    private void connectSocket() {
        socket = App.getPartyappSocket();
        socket.connect();
        socket.off();
    }

    //TODO add apikey server-side
    private void setupSocket() {

        final Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        //TODO handle new person in the current party as an event?
        socket.on("playlist", args -> {
            JSONObject rootPlaylist = (JSONObject) args[0];
            playlist = gson.fromJson(rootPlaylist.toString(), Playlist.class);
            for (Song s : playlist.getPlaylist()) {
                Log.d(TAG, s.toString());
            }
            runOnUiThread(() -> updatePlaylist(playlist));
        });

        socket.on("playTrack", args -> {
            JSONObject root = (JSONObject) args[0];
            try {
                JSONObject currentTrackJSON = root.getJSONObject("song");
                Log.d(TAG, "JSON:" + currentTrackJSON.toString());
                currentTrack = gson.fromJson(currentTrackJSON.toString(), Song.class);
                currentTrack = Utils.checkUrls(currentTrack);
                Log.i(TAG, "Current Track:" + currentTrack.getTitle());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            runOnUiThread(() -> setCurrentSong(currentTrack));
        });
    }

    private void updatePlaylist(Playlist playlist) {
        Log.i(TAG, "Updating playlist");
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(playListAdapter);
        playListAdapter.clearPlaylist();
        playListAdapter.addSongs(playlist.getPlaylist());
        playListAdapter.notifyDataSetChanged();
    }

    private void setCurrentSong(Song song) {
        Log.d(TAG, "setCurrentSong() called with: " + song);
        Picasso.with(this).load(song.getArtistImage()).error(R.drawable.album_placeholder).into(imgArtist); //TODO find a great artist placeholder
        Picasso.with(this).load(song.getTrackImage()).error(R.drawable.album_placeholder).into(imgSong);
        txtTitle.setText(song.getTitle());
        txtArtist.setText(song.getArtist());
        Picasso.with(this).load(song.getUserImage()).placeholder(R.drawable.user_placeholder).noFade().into(imgUser);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                //showSetting();
                break;
            case R.id.action_leave_party:
                leaveParty();
                break;
            case R.id.action_previous_songs:
                startPreviousSongsActivity();
                break;
            case R.id.action_party_details:
                startDetailsParty();
                break;
            default:
                break;
        }
        return true;
    }

    private void startDetailsParty() {
        Log.d(TAG, "Detail party clicked");
        Intent startDetailsPartyIntent = new Intent(this, PartyDetailsActivity.class);
        startDetailsPartyIntent.putExtra("party_id", partyAppProfile.getPartyID());
        startActivity(startDetailsPartyIntent);
    }

    private void startPreviousSongsActivity() {
        Log.d(TAG, "Previous songs clicked");
        Intent startPreviousSongsIntent = new Intent(this, SongPreviousActivity.class);
        startPreviousSongsIntent.putExtra("party_id", partyAppProfile.getPartyID());
        startActivity(startPreviousSongsIntent);
    }

    private void leaveParty() {
        Log.d(TAG, "Leave party clicked");
        partyAppProfile.leaveParty();
        Log.d("PartyAppProfile","Leaving the party, destroying the socket");
        App.destroyCurrentPartyappSocket();
        Storage.savePartyAppProfile(getApplicationContext(), partyAppProfile);
        startPartyLoginActivity();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!socket.connected())
            socket.connect();
    }

    //socket is set to null when party is left
    //socket disconnects when app goes in background
    @Override
    protected void onStop() {
        super.onStop();
        if (socket != null && socket.connected()) {
            socket.disconnect();
        }
    }

    private void startSearchSongActivity() {
        Intent intent = new Intent(this, SongSearchActivity.class);
        startActivityForResult(intent, GET_SONG_REQUEST);
    }

    @DebugLog
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult() called with: " + "requestCode = [" + requestCode + "], resultCode = [" + resultCode + "], data = [" + data + "]");
        if (requestCode == GET_SONG_REQUEST) {
            if (resultCode == RESULT_OK) {
                SongResult song = data.getParcelableExtra("Song");
                if (song != null) {
                    insertSong(song);
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.d(TAG, "User didn't pick a song");
            }
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    private void insertSong(SongResult song) {
        App.getRestClient().getApiService().insertSong(song.getArtist(), song.getTrackID(), song.getImgURL(), song.getTitle(), partyAppProfile.getPartyID(), partyAppProfile.getIdUser())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<InsertSongResponse>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError();
                    }

                    @Override
                    public void onNext(InsertSongResponse insertSongResponse) {
                        //song added successfully
                        //song was not insert in queue
                        Log.d(TAG, insertSongResponse.getMessage());
                        if (insertSongResponse.getMessage().equals("success")) {
                            Log.i(TAG, song.getTitle() + " added to playlist");
                        } else {
                            showError();
                        }
                    }
                });
    }

    private void showError() {
        //// FIXME: 06/05/2016 if user has added > 3 songs api returns an error
        Log.w(TAG, "Something went wrong while adding the song");
        SnackbarFactory.make(recyclerView, "Failed to add the song, try again later", R.color.primary, Snackbar.LENGTH_LONG);
    }
}
