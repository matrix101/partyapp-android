package com.devchimps.partyapp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.UserListAdapter;
import com.devchimps.partyapp.model.User;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MGrim on 09/08/2015.
 */
public class PartyDetailsActivity extends BaseActivity {

    @BindView(R.id.rv_party_details)
    SuperRecyclerView usersRecyclerView;
    @BindView(R.id.txt_empty_message)
    TextView txtEmptyMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_party_details);
        ButterKnife.bind(this);

        initToolbar();
        initRecyclerView();

    }

    private void initRecyclerView() {
        txtEmptyMessage.setText(R.string.empty_message_no_parties);
        usersRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        User testUser = new User("621cb67fb1bd27a92930e7b73aec45d0", "Alessandro Ferrara", "https://lh4.googleusercontent.com/-3acbHeL0Ym4/AAAAAAAAAAI/AAAAAAAAADQ/8KtXErTegzE/photo.jpg?sz=150");
        ArrayList<User> users = new ArrayList<>();
        users.add(testUser);
        usersRecyclerView.setAdapter(new UserListAdapter(users, getApplicationContext()));
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbarTitle.setText(R.string.toolbar_users);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }
}
