package com.devchimps.partyapp.activity;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.ProfileViewPagerAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.network.model.UserProfileResponse;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.AnimationUtility;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Francesco on 05/07/2015.
 */
public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_value_parties)
    TextView txtTotalParties;
    @BindView(R.id.txt_value_songs)
    TextView txtTotalSongs;
    @BindView(R.id.tabs_user_profile)
    TabLayout tlUserProfileTabs;
    @BindView(R.id.pager)
    ViewPager viewPager;

    private PartyAppProfile partyAppProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        //String profileID = savedInstanceState.getString("profile_id");
        //TODO servono le api social per modificare questa activity
        //getUserByID per mostrare il profilo di un altro utente

        partyAppProfile = Storage.getPartyappProfile(this);

        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);

        initToolbar();
        toolbarTitle.setText(R.string.toolbar_title_profile);

        if (partyAppProfile != null) {
            Picasso.with(this).load(partyAppProfile.getImageURL()).into(imgProfile);
            txtName.setText(partyAppProfile.getFullName());
            App.getRestClient().getApiService().getUserProfile(partyAppProfile.getIdUser())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<UserProfileResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e(TAG, "No internet connection");
                            //send stop loading and show no internet connection message
                            showNoConnectionDialog();
                        }


                        @Override
                        public void onNext(UserProfileResponse userProfileResponse) {
                            AnimationUtility.animationCountTo(0, userProfileResponse.getUserStats().getTotalSongs(), txtTotalSongs);
                            AnimationUtility.animationCountTo(0, userProfileResponse.getUserStats().getTotalParties(), txtTotalParties);
                        }
                    });
        }
        setupViewPager();
    }

    private void showNoConnectionDialog() {
        //TODO create a better message
        new MaterialDialog.Builder(this)
                .title("Warning")
                .content("No internet connection")
                .positiveText("Ok")
                .onPositive((dialog1, which) -> onBackPressed())
                .show();
    }

    private void setupViewPager() {
        viewPager.setAdapter(new ProfileViewPagerAdapter(getSupportFragmentManager(), partyAppProfile.getIdUser()));
        tlUserProfileTabs.setupWithViewPager(viewPager);
    }

    private void initToolbar() {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/comfortaa_regular.ttf");
        toolbarTitle.setTypeface(font);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                showNoConnectionDialog();
                //logut();
                //TODO logout from social (facebook or google)
                break;
            case R.id.action_setting:
                showNoConnectionDialog();
                //showSetting();
                break;
            default:
                break;
        }
        return true;
    }
}