package com.devchimps.partyapp.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.devchimps.partyapp.R;

import butterknife.BindView;

/**
 * Created by Maria on 30/07/2015.
 */

//TODO this class
public class SongDetailsActivity extends BaseActivity {
    @BindView(R.id.contentRoot)
    LinearLayout contentRoot;
    @BindView(R.id.img_song)
    ImageView imgSong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_details);

    }
}
