package com.devchimps.partyapp.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by MGrim on 09/08/2015.
 */
public class SongPreviousActivity extends BaseActivity {

    private static final String TAG = "SongPreviousActivity";
    @BindView(R.id.rv_previous_songs)
    SuperRecyclerView previousSongsRecyclerView;
    @BindView(R.id.txt_empty_message)
    TextView txtEmptyMessage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_previous_songs);
        ButterKnife.bind(this);
        initToolbar();
        initRecyclerView();
    }

    @Override
    protected void initToolbar() {
        super.initToolbar();
        toolbarTitle.setText("Previous songs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
    }

    private void initRecyclerView() {
        txtEmptyMessage.setText("No previous songs");
        previousSongsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        //TODO the whole class
        //previousSongsRecyclerView.setAdapter(new PlayListAdapter(new ArrayList<>(), getApplicationContext()));
        /*ArrayList<String> test = new ArrayList<>();
        test.add("ciao");
        test.add("lol");
        previousSongsRecyclerView.setAdapter(new SwipeAdapter(test));*/
    }
}
