package com.devchimps.partyapp.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.ArraySearchAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.network.model.SongResult;
import com.devchimps.partyapp.storage.Storage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Maria on 30/07/2015.
 */
public class SongSearchActivity extends AppCompatActivity implements TextView.OnEditorActionListener, AdapterView.OnItemClickListener {

    private static final String TAG = "SeachSongActivity";
    @BindView(R.id.etxt_search)
    EditText editText;

    @BindView(R.id.list_songs_result)
    ListView listView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.loadingPanel)
    RelativeLayout loadingPanel;
    @BindView(R.id.errorPanel)
    RelativeLayout errorPanel;
    @BindView(R.id.txt_error_message)
    TextView txtErrorMessage;

    private ArraySearchAdapter listAdapter;
    private ArrayList<SongResult> data;
    private PartyAppProfile partyAppProfile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_search);
        ButterKnife.bind(this);
        setUnderlineColorEditText();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        partyAppProfile = Storage.getPartyappProfile(this);

        data = new ArrayList<>();
        listAdapter = new ArraySearchAdapter(this, R.layout.item_search, data);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(this);
        editText.setOnEditorActionListener(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void setUnderlineColorEditText() {
        editText.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            loadingPanel.setVisibility(View.VISIBLE);
            listAdapter.clear();
            String query = editText.getText().toString().trim();
            if (partyAppProfile != null) {
                searchSong(query);
            } else {
                Log.e(TAG, "PartyAppProfile is null");
            }
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SongResult song = data.get(position);
        Log.i(TAG, "Song clicked: " + song.getTitle());
        Intent intent = getIntent();
        intent.putExtra("Song", song);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void searchSong(String query) {
        App.getRestClient().getApiService().getSongs(query, partyAppProfile.getIdUser())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    hideError();
                    hideSongs();
                    showLoading();
                })
                .subscribe(new Subscriber<List<SongResult>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        hideLoading();
                        showError("No internet connection");
                    }

                    @Override
                    public void onNext(List<SongResult> songResults) {
                        hideLoading();
                        showSongs(songResults);
                    }
                });
    }


    private void showLoading() {
        loadingPanel.setVisibility(View.VISIBLE);

    }

    private void hideLoading() {
        loadingPanel.setVisibility(View.GONE);
    }

    private void showError(String message) {
        txtErrorMessage.setText(message);
        errorPanel.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        errorPanel.setVisibility(View.GONE);
    }

    private void showSongs(List<SongResult> results) {
        loadingPanel.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        if (results == null) {
            showError("No internet connection");
        } else if (results.size() == 0) {
            showError("No songs found");
        } else listAdapter.addAll(results);
    }

    private void hideSongs() {
        listView.setVisibility(View.GONE);
        listAdapter.clear();

    }
}
