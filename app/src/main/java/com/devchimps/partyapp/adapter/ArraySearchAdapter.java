package com.devchimps.partyapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.network.model.SongResult;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Francesco on 20/06/2015.
 */
public class ArraySearchAdapter extends ArrayAdapter<SongResult> {

    private static final String TAG = "ArraySearchAdapter";
    private Context context;
    private int layoutResourceId;
    private List<SongResult> data;

    public ArraySearchAdapter(Context context, int layoutResourceId, List<SongResult> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ItemSearchHolder holder;

        if (view != null) {
            holder = (ItemSearchHolder) view.getTag();
        } else {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            view = inflater.inflate(layoutResourceId, parent, false);
            holder = new ItemSearchHolder(view);
            view.setTag(holder);
        }

        final SongResult song = data.get(position);
        if (song.getImgURL() != null && !song.getImgURL().equals(""))
            Picasso.with(context).load(song.getImgURL()).placeholder(R.drawable.album_placeholder).into(holder.imgSong);
        else
            Picasso.with(context).load(R.drawable.album_placeholder);
        holder.txtTitle.setText(song.getTitle());
        holder.txtArtist.setText(song.getArtist());
        return view;
    }

    static class ItemSearchHolder {
        @BindView(R.id.img_song)
        ImageView imgSong;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_artist)
        TextView txtArtist;

        public ItemSearchHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
