package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.activity.MainActivity;
import com.devchimps.partyapp.model.Party;
import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class PartyAdapter extends RecyclerView.Adapter<PartyAdapter.PartyHolder> {
    private static final String TAG = "PartyAdapter";
    private final List<Party> parties;
    private Context context;
    private PartyAppProfile partyAppProfile;

    public PartyAdapter(List<Party> parties, Context context) {
        this.parties = parties;
        this.context = context;
        partyAppProfile = Storage.getPartyappProfile(context);
    }

    @Override
    public PartyHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(context).inflate(R.layout.item_party, viewGroup, false);
        return new PartyHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final PartyHolder holder, final int position) {
        final Party party = parties.get(position);
        holder.txtPartyName.setText(party.getName());
        holder.txtPartyOwner.setText(party.getUser().getName());
        holder.txtPartyLocation.setText(party.getAddress());
        holder.imgPartyIcon.setOnClickListener(v -> {
            //fixme add popup asking if he wants to open googlemaps
            startMapsActivity(party);
        });
        bindPartyImage(holder, position);
        Picasso.with(context).load(party.getUser().getImgURL()).into(holder.imgPartyOwner);

        holder.itemView.setOnClickListener(v -> {
            Log.d(TAG, "OnClick " + party.getId());
            partyAppProfile.joinParty(party.getId());
            Storage.savePartyAppProfile(context, partyAppProfile);
            startMainActivity(party);

        });
    }


    private void startMapsActivity(Party party) {
        Uri mapUri = Uri.parse("google.navigation:q=" + Uri.encode(party.getAddress()));
        Log.i(TAG, "Map uri:" + mapUri);
        Intent mapIntent = new Intent(android.content.Intent.ACTION_VIEW, mapUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mapIntent);
    }

    private void startMainActivity(Party party) {
        Log.i(TAG, "Joined " + party.getId() + " party, Starting mainActivity");
        Intent startMainActivityIntent = new Intent(context, MainActivity.class);
        startMainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startMainActivityIntent);
    }

    private void bindPartyImage(final PartyHolder holder, int position) {
        Party currentParty = parties.get(position);

        String mapboxImg = Constants.getMapBoxImgUrl(currentParty.getPosition().getCoordinates()[0], currentParty.getPosition().getCoordinates()[1], 600, 600);

        Picasso.with(context)
                .load(mapboxImg)
                .fit().centerCrop()
                .into(holder.imgParty);
    }


    @Override
    public int getItemCount() {
        return parties.size();
    }

    public class PartyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_party)
        ImageView imgParty;
        @BindView(R.id.txt_party_owner)
        TextView txtPartyOwner;
        @BindView(R.id.txt_party_name)
        TextView txtPartyName;
        @BindView(R.id.txt_party_location)
        TextView txtPartyLocation;
        @BindView(R.id.img_party_icon)
        ImageView imgPartyIcon;
        @BindView(R.id.img_party_owner)
        CircleImageView imgPartyOwner;

        public PartyHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}



