package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.activity.ProfileActivity;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.model.Song;
import com.devchimps.partyapp.network.model.LikeResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.ItemPlaylistHolder> {
    private static final String TAG = "PlayListAdapter";
    private final List<Song> playlist;
    private Context context;
    private String userID;

    public PlayListAdapter(List<Song> playlist, Context context, String userID) {
        this.userID = userID;
        this.playlist = playlist;
        this.context = context;
    }

    @Override
    public ItemPlaylistHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_playlist, viewGroup, false);
        return new ItemPlaylistHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final ItemPlaylistHolder holder, final int position) {

        final Song song = playlist.get(position);
        if (song.getTrackImage() == null || song.getTrackImage().equals(""))
            Picasso.with(context).load(R.drawable.album_placeholder).into(holder.imgSong);
        else
            Picasso.with(context).load(song.getTrackImage()).error(R.drawable.album_placeholder).into(holder.imgSong);
        holder.txtTitle.setText(song.getTitle());
        holder.txtArtist.setText(song.getArtist());
        holder.txtUser.setText(song.getUsername());
        holder.txtCountLike.setText("" + song.getLikeCount());

        boolean songLiked = false;
        for (String user : song.getListLikes()) {
            if (user.equals(userID)) {
                songLiked = true;
                Picasso.with(context).load(R.drawable.ic_favorite_white_24dp).into(holder.imgCountLike);
                break;
            }
        }
        if (!songLiked)
            Picasso.with(context).load(R.drawable.ic_favorite_outline_white_24dp).into(holder.imgCountLike);



        if (song.getUserImage() == null || song.getUserImage().equals(""))
            Picasso.with(context).load(R.drawable.user_placeholder).into(holder.imgUser);
        else
            Picasso.with(context).load(song.getUserImage()).noFade().into(holder.imgUser);

        holder.imgUser.setOnClickListener(v -> startProfileActivity(song.getuserID()));

        holder.imgCountLike.setOnClickListener(v -> App.getRestClient().getApiService().likeSong(song.getTrackId(), song.getPartyId(), userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LikeResponse>() {
                    @Override
                    public void onCompleted() {
                        //update view?
                        //right now the list is updated by every event from websocket
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.w(TAG, "failed to like " + holder.txtTitle.getText());
                    }

                    @Override
                    public void onNext(LikeResponse likeResponse) {
                        Log.i(TAG, likeResponse.getMessage());
                        if (likeResponse.getMessage().equals("success")) {
                            //TODO do something
                        } else {
                            //TODO something went wrong
                        }
                    }
                }));

        holder.imgUser.setOnClickListener(v -> startProfileActivity(song.getuserID()));
    }


    private void startProfileActivity(String profileID) {
        Intent startProfileActivityIntent = new Intent(context, ProfileActivity.class);
        startProfileActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startProfileActivityIntent.putExtra("profile_id", profileID);
        context.startActivity(startProfileActivityIntent);
    }

    @Override
    public int getItemCount() {
        return playlist.size();
    }

    public void addSongs(List<Song> songs) {
        this.playlist.addAll(songs);
    }

    public void clearPlaylist() {
        this.playlist.clear();
    }

    public class ItemPlaylistHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_song)
        ImageView imgSong;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_artist)
        TextView txtArtist;
        @BindView(R.id.txt_user)
        TextView txtUser;
        @BindView(R.id.img_user)
        CircleImageView imgUser;
        @BindView(R.id.img_count_like)
        ImageView imgCountLike;
        @BindView(R.id.txt_count_like)
        TextView txtCountLike;

        public ItemPlaylistHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}



