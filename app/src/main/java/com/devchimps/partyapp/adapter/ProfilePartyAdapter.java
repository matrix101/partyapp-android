package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.model.Party;
import com.devchimps.partyapp.util.Constants;
import com.devchimps.partyapp.util.SnackbarFactory;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfilePartyAdapter extends RecyclerView.Adapter<ProfilePartyAdapter.ProfilePartyHolder> {
    private static final String TAG = "ProfilePartyAdapter";
    private final List<Party> parties;
    private Context context;

    public ProfilePartyAdapter(List<Party> parties, Context context) {
        this.parties = parties;
        this.context = context;
    }

    @Override
    public ProfilePartyHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_profile_party, viewGroup, false);
        return new ProfilePartyHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final ProfilePartyHolder holder, final int position) {
        final Party party = parties.get(position);
        holder.txtPartyName.setText(party.getName());
        holder.txtPartyOwner.setText(party.getUser().getName());
        holder.txtPartyLocation.setText(party.getAddress());
        bindPartyImage(holder, position);
        holder.itemView.setOnClickListener(v -> {
            //TODO detailparty activity?
            //TODO show what happened in this party?
            SnackbarFactory.make(holder.itemView, "Not implemeted yet.", R.color.primary, Snackbar.LENGTH_LONG);
            Log.d(TAG, "OnClick " + party.getId());
        });
    }

    private void bindPartyImage(final ProfilePartyHolder holder, int position) {
        Party currentParty = parties.get(position);
        String mapboxImg = Constants.getMapBoxImgUrl(currentParty.getPosition().getCoordinates()[0], currentParty.getPosition().getCoordinates()[1], 600, 600);

        Picasso.with(context)
                .load(mapboxImg)
                .fit().centerCrop()
                .into(holder.imgParty);
    }

    @Override
    public int getItemCount() {
        return parties.size();
    }

    public class ProfilePartyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_party)
        ImageView imgParty;
        @BindView(R.id.txt_party_name)
        TextView txtPartyName;
        @BindView(R.id.txt_party_owner)
        TextView txtPartyOwner;
        @BindView(R.id.txt_party_location)
        TextView txtPartyLocation;

        public ProfilePartyHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }
    }
}