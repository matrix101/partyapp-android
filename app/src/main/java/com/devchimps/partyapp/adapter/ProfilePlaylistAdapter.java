package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.model.Song;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ProfilePlaylistAdapter extends RecyclerView.Adapter<ProfilePlaylistAdapter.ItemPlaylistHolder> {
    private static final String TAG = "ProfilePlaylistAdapter";
    private final List<Song> playlist;
    private Context context;
    private String userID;

    public ProfilePlaylistAdapter(List<Song> playlist, Context context, String userID) {
        this.userID = userID;
        this.playlist = playlist;
        this.context = context;
    }

    @Override
    public ItemPlaylistHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_profile_playlist, viewGroup, false);
        return new ItemPlaylistHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final ItemPlaylistHolder holder, final int position) {

        final Song song = playlist.get(position);
        if (song.getTrackImage() == null || song.getTrackImage().equals(""))
            Picasso.with(context).load(R.drawable.album_placeholder).into(holder.imgSong);
        else
            Picasso.with(context).load(song.getTrackImage()).error(R.drawable.album_placeholder).into(holder.imgSong);
        holder.txtTitle.setText(song.getTitle());
        holder.txtArtist.setText(song.getArtist());
        holder.txtCountLike.setText("" + song.getLikeCount());
        boolean songLiked = false;
        //if(song.getListLikes().)
        for (String user : song.getListLikes()) {
            if (user.equals(userID)) {
                songLiked = true;
                Picasso.with(context).load(R.drawable.ic_favorite_white_24dp).into(holder.imgCountLike);
                break;
            }
        }
        if (!songLiked)
            Picasso.with(context).load(R.drawable.ic_favorite_outline_white_24dp).into(holder.imgCountLike);

    }

    @Override
    public int getItemCount() {
        return playlist.size();
    }

    public void addSongs(List<Song> songs) {
        this.playlist.addAll(songs);
    }

    public void clearPlaylist() {
        this.playlist.clear();
    }

    public class ItemPlaylistHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_song)
        ImageView imgSong;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_artist)
        TextView txtArtist;
        @BindView(R.id.img_count_like)
        ImageView imgCountLike;
        @BindView(R.id.txt_count_like)
        TextView txtCountLike;

        public ItemPlaylistHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

