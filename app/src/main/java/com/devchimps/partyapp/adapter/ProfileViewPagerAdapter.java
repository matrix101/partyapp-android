package com.devchimps.partyapp.adapter;

/**
 * Created by Maria on 31/07/2015.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.devchimps.partyapp.fragment.ProfilePartiesFragment;
import com.devchimps.partyapp.fragment.ProfilePlaylistFragment;
import com.devchimps.partyapp.fragment.TextFragment;

public class ProfileViewPagerAdapter extends FragmentPagerAdapter {

    private int pagerCount = 2;
    private String userID;

    public ProfileViewPagerAdapter(FragmentManager fm, String userID) {
        super(fm);
        this.userID = userID;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ProfilePlaylistFragment.newInstance(userID);
            case 1:
                return ProfilePartiesFragment.newInstance(userID);
            default:
                return TextFragment.newInstance("hello");
        }
    }

    @Override
    public int getCount() {
        return pagerCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Songs";
            case 1:
                return "Party";
            default:
                return "";
        }
    }

}
