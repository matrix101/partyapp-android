package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.network.model.SongResult;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ItemSearchHolder> {
    private static final String TAG = "SearchAdapter";
    private final List<SongResult> songs;
    private Context context;

    public SearchAdapter(List<SongResult> songs, Context context) {
        this.songs = songs;
        this.context = context;
    }

    @Override
    public ItemSearchHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search, viewGroup, false);
        return new ItemSearchHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final ItemSearchHolder holder, final int position) {

        final SongResult song = songs.get(position);
        if (song.getImgURL() == null || song.getImgURL().equals(""))
            Picasso.with(context).load(R.drawable.album_placeholder).into(holder.imgSong);
        else
            Picasso.with(context).load(song.getImgURL()).error(R.drawable.album_placeholder).into(holder.imgSong);
        holder.txtTitle.setText(song.getTitle());
        holder.txtArtist.setText(song.getArtist());
    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public void addSongs(List<SongResult> songs) {
        this.songs.addAll(songs);
    }

    public void clearPlaylist() {
        songs.clear();
    }

    public class ItemSearchHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_song)
        ImageView imgSong;
        @BindView(R.id.txt_title)
        TextView txtTitle;
        @BindView(R.id.txt_artist)
        TextView txtArtist;

        public ItemSearchHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}



