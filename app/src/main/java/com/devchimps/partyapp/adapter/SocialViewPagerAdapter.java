package com.devchimps.partyapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.devchimps.partyapp.fragment.PagerFragment;

/**
 * Created by Matrix on 06/07/2015.
 */
public class SocialViewPagerAdapter extends FragmentPagerAdapter {

    private int pagerCount = 5;

    public SocialViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        return PagerFragment.newInstance(i, "hello");
    }

    @Override
    public int getCount() {
        return pagerCount;
    }
}
