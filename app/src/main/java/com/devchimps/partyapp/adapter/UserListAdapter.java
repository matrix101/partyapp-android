package com.devchimps.partyapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.activity.ProfileActivity;
import com.devchimps.partyapp.model.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by MGrim on 10/08/2015.
 */
public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.UserViewHolder> {
    private static final String TAG = "UserListAdapter";
    private final List<User> users;
    private Context context;

    public UserListAdapter(List<User> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View rowView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user, viewGroup, false);
        return new UserViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {

        final User user = users.get(position);

        if (user.getImgURL() == null || user.getImgURL().equals(""))
            Picasso.with(context).load(R.drawable.user_placeholder).into(holder.imgUser);
        else
            Picasso.with(context).load(user.getImgURL()).error(R.drawable.album_placeholder).into(holder.imgUser);
        holder.txtName.setText(user.getName());

    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    private void startProfileActivity(String profileID) {
        Intent startProfileActivityIntent = new Intent(context, ProfileActivity.class);
        startProfileActivityIntent.putExtra("profile_id", profileID);
        context.startActivity(startProfileActivityIntent);
    }

    public class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_user)
        CircleImageView imgUser;
        @BindView(R.id.txt_name)
        TextView txtName;

        public UserViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "user clicked: " + getAdapterPosition());
            // TODO: 26/11/2015 show user profile
        }
    }
}
