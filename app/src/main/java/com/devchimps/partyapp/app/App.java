package com.devchimps.partyapp.app;

import android.app.Application;
import android.content.Context;

import com.devchimps.partyapp.network.PartyappSocket;
import com.devchimps.partyapp.network.RestClient;


/**
 * Created by Matrix on 28/02/2015.
 */
public class App extends Application {
    private static RestClient restClient;
    private static Context context;
    private static PartyappSocket socket;

    //TODO global socket?

    public static RestClient getRestClient() {
        if(restClient == null) {
            restClient = new RestClient(App.context);
        }
        return restClient;
    }

    public static PartyappSocket getPartyappSocket() {
        if (socket == null) {
            socket = new PartyappSocket(context);
        }
        return socket;
    }

    public static void destroyCurrentPartyappSocket() {
        socket = null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //LeakCanary.install(this);
        App.context = getApplicationContext();
        restClient = getRestClient();
        //socket = getPartyappSocket();
    }
}
