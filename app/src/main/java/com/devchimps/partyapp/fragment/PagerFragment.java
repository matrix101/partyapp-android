package com.devchimps.partyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Matrix on 24/05/2015.
 */
public class PagerFragment extends Fragment {
    private static final String KEY_IMAGE = "image";
    private static final String KEY_MESSAGE = "message";
    @BindView(R.id.img_holder)
    ImageView imgHolder;
    @BindView(R.id.txt_holder)
    TextView txtHolder;


    public static Fragment newInstance(int imgId, String message) {
        PagerFragment f = new PagerFragment();
        Bundle args = new Bundle();

        args.putInt(KEY_IMAGE, imgId);
        args.putString(KEY_MESSAGE, message);
        f.setArguments(args);

        return (f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login_intro, container, false);
        ButterKnife.bind(this, view);
        Picasso.with(getActivity().getApplicationContext()).load(R.drawable.logo).into(imgHolder);
        txtHolder.setText(R.string.pager_welcome_message);
        return view;
    }
}
