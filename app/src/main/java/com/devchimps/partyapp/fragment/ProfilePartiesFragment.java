package com.devchimps.partyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.ProfilePartyAdapter;
import com.devchimps.partyapp.app.App;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matrix on 24/05/2015.
 */
public class ProfilePartiesFragment extends Fragment {

    private static final String TAG = "ProfilePartiesFragment";
    @BindView(R.id.rv_profile_parties)
    RecyclerView recyclerViewParties;
    @BindView(R.id.loadingPanel)
    RelativeLayout loadingPanel;
    @BindView(R.id.errorPanel)
    RelativeLayout errorPanel;
    @BindView(R.id.txt_error_message)
    TextView txtErrorMessage;

    public static Fragment newInstance(String userID) {
        ProfilePartiesFragment f = new ProfilePartiesFragment();
        Bundle args = new Bundle();
        args.putString("userID", userID);
        f.setArguments(args);
        return (f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_parties, container, false);
        ButterKnife.bind(this, rootView);
        String userID = getArguments().getString("userID");
        recyclerViewParties.setLayoutManager(new LinearLayoutManager(getActivity()));
        getUserParties(userID);
        return rootView;
    }

    private void getUserParties(String userID) {
        App.getRestClient().getApiService().getUserParties(userID)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(() -> {
                    errorPanel.setVisibility(View.GONE);
                    loadingPanel.setVisibility(View.VISIBLE);
                    recyclerViewParties.setVisibility(View.GONE);
                })
                .onErrorReturn(err -> {
                    Log.e(TAG, "No internet connection: " + err.getMessage());
                    return null;
                })
                .doOnCompleted(() -> recyclerViewParties.setVisibility(View.VISIBLE))
                .subscribe(results -> {
                    loadingPanel.setVisibility(View.GONE);
                    if (results == null) {
                        txtErrorMessage.setText("No internet connection");
                        errorPanel.setVisibility(View.VISIBLE);
                    } else if (results.getMessage().equalsIgnoreCase("success")) {
                        ProfilePartyAdapter partyAdapter = new ProfilePartyAdapter(results.getUserParties(), getContext());
                        recyclerViewParties.setAdapter(partyAdapter);
                        if (results.getUserParties().size() == 0) {
                            txtErrorMessage.setText("No parties");
                            errorPanel.setVisibility(View.VISIBLE);
                        }
                    }
                });
    }
}
