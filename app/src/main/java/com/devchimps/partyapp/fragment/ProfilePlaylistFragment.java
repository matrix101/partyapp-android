package com.devchimps.partyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devchimps.partyapp.R;
import com.devchimps.partyapp.adapter.ProfilePlaylistAdapter;
import com.devchimps.partyapp.app.App;
import com.devchimps.partyapp.model.Song;
import com.devchimps.partyapp.network.model.UserPlaylistResponse;
import com.malinskiy.superrecyclerview.SuperRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Matrix on 24/05/2015.
 */
public class ProfilePlaylistFragment extends Fragment {

    private static final String TAG = "ProfilePlaylistFragment";
    @BindView(R.id.rv_profile_playlist)
    SuperRecyclerView recyclerViewPlaylist;
    @BindView(R.id.txt_empty_message)
    TextView txtEmptyMessage;

    private ProfilePlaylistAdapter playListAdapter;
    private int skip = 0;
    private int limit = 25;

    public static Fragment newInstance(String userID) {
        ProfilePlaylistFragment f = new ProfilePlaylistFragment();
        Bundle args = new Bundle();
        args.putString("userID", userID);
        f.setArguments(args);
        return (f);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile_songs, container, false);
        ButterKnife.bind(this, rootView);
        String userID = getArguments().getString("userID");
        getUserPlaylist(userID);
        skip = skip + 50;
        List<Song> playlist = new ArrayList<>();

        txtEmptyMessage.setText("No songs added");
        recyclerViewPlaylist.setLayoutManager(new LinearLayoutManager(getActivity()));
        playListAdapter = new ProfilePlaylistAdapter(playlist, getActivity().getApplicationContext(), userID);

        recyclerViewPlaylist.setupMoreListener((numberOfItems, numberBeforeMore, currentItemPos) -> {
            Log.d(TAG, "numberOfItems: " + numberOfItems + " numberBeforeMore: " + numberBeforeMore + " currentItemPos: " + currentItemPos);
            getUserPlaylist(userID);
            skip = skip + 50;
        }, 5);

        return rootView;
    }

    private void getUserPlaylist(String userID) {
        App.getRestClient().getApiService().getUserPlaylist(userID, skip, limit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UserPlaylistResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserPlaylistResponse userPlaylistResponse) {
                        if (userPlaylistResponse.getMessage().equals("success")) {
                            Log.i(TAG, "Retrived user playlist successfully");
                            if (recyclerViewPlaylist.getAdapter() == null)
                                recyclerViewPlaylist.setAdapter(playListAdapter);
                            playListAdapter.addSongs(userPlaylistResponse.getUserPlaylist());
                            playListAdapter.notifyDataSetChanged();
                            if (userPlaylistResponse.getUserPlaylist().size() < limit) {
                                recyclerViewPlaylist.removeMoreListener();
                                recyclerViewPlaylist.hideMoreProgress();
                            }
                        } else {
                            Log.i(TAG, "onNext: Something went wrong");
                        }
                    }
                });
    }


}
