package com.devchimps.partyapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Matrix on 24/05/2015.
 */
public class TextFragment extends Fragment {
    private static final String KEY_TITLE = "title";

    public static Fragment newInstance(String title) {
        TextFragment f = new TextFragment();
        Bundle args = new Bundle();


        args.putString(KEY_TITLE, title);
        f.setArguments(args);

        return (f);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        TextView text = new TextView(getActivity());
        text.setText("Fragment content");
        text.setGravity(Gravity.CENTER);
        return text;
    }
}
