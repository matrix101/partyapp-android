package com.devchimps.partyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MGrim on 07/08/2015.
 */
public class Parties implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Parties> CREATOR = new Parcelable.Creator<Parties>() {
        @Override
        public Parties createFromParcel(Parcel in) {
            return new Parties(in);
        }

        @Override
        public Parties[] newArray(int size) {
            return new Parties[size];
        }
    };
    private List<Party> parties;

    public Parties(List<Party> parties) {
        this.parties = parties;
    }

    private Parties(Parcel in) {
        if (in.readByte() == 0x01) {
            parties = new ArrayList<>();
            in.readList(parties, Party.class.getClassLoader());
        } else {
            parties = null;
        }
    }

    public List<Party> getParties() {
        return parties;
    }

    public void setParties(List<Party> parties) {
        this.parties = parties;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (parties == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(parties);
        }
    }
}
