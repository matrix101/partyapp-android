package com.devchimps.partyapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.Date;

/**
 * Created by Matrix on 15/06/2015.
 */
public class Party {
    @SerializedName("party_id")
    private
    String id;
    @SerializedName("name")
    private
    String name;
    @SerializedName("date")
    private
    Date startDate;
    @SerializedName("active")
    private
    boolean isPartyActive;
    @SerializedName("address")
    private
    String address;
    @SerializedName("public")
    private
    boolean isPartyPublic;
    @SerializedName("position")
    private
    Position position;
    @SerializedName("user")
    private
    User user;
    //TODO lista utenti che hanno partecipato al party?


    public Party(String id, String name, Date startDate, boolean isPartyActive, String address, boolean isPartyPublic, Position position, User user) {
        this.id = id;
        this.name = name;
        this.startDate = startDate;
        this.isPartyActive = isPartyActive;
        this.address = address;
        this.isPartyPublic = isPartyPublic;
        this.position = position;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public boolean isPartyActive() {
        return isPartyActive;
    }

    public void setIsPartyActive(boolean isPartyActive) {
        this.isPartyActive = isPartyActive;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isPartyPublic() {
        return isPartyPublic;
    }

    public void setIsPartyPublic(boolean isPartyPublic) {
        this.isPartyPublic = isPartyPublic;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Party{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", startDate=" + startDate +
                ", isPartyActive=" + isPartyActive +
                ", address='" + address + '\'' +
                ", isPartyPublic=" + isPartyPublic +
                ", position=" + position +
                ", user='" + user + '\'' +
                '}';
    }

    public class Position {
        @SerializedName("type")
        String type;
        @SerializedName("coordinates")
        float[] coordinates;

        public Position(String type, float[] coordinates) {
            this.type = type;
            this.coordinates = coordinates;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public float[] getCoordinates() {
            return coordinates;
        }

        public void setCoordinates(float[] coordinates) {
            this.coordinates = coordinates;
        }

        @Override
        public String toString() {
            return "Position{" +
                    "type='" + type + '\'' +
                    ", coordinates=" + Arrays.toString(coordinates) +
                    '}';
        }
    }
}
