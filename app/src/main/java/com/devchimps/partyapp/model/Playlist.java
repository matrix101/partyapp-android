package com.devchimps.partyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Matrix on 18/06/2015.
 */
public class Playlist implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Playlist> CREATOR = new Parcelable.Creator<Playlist>() {
        @Override
        public Playlist createFromParcel(Parcel in) {
            return new Playlist(in);
        }

        @Override
        public Playlist[] newArray(int size) {
            return new Playlist[size];
        }
    };
    @SerializedName("playlist")
    private List<Song> playlist;

    public Playlist(List<Song> playlist) {
        this.playlist = playlist;
    }

    public Playlist() {
        this.playlist = new ArrayList<>();
    }

    protected Playlist(Parcel in) {
        if (in.readByte() == 0x01) {
            playlist = new ArrayList<>();
            in.readList(playlist, Song.class.getClassLoader());
        } else {
            playlist = null;
        }
    }

    public List<Song> getPlaylist() {
        return playlist;
    }

    public void setPlaylist(List<Song> playlist) {
        this.playlist = playlist;
    }

    public void addSong(Song song) {
        playlist.add(song);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (playlist == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(playlist);
        }
    }
}