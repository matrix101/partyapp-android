package com.devchimps.partyapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

    /*
    {
      "party_id": "E1m9O-Xq",
      "user_id": "621cb67fb1bd27a92930e7b73aec45d0",
      "username": "Alessandro",
      "userimage": "https://lh4.googleusercontent.com/-3acbHeL0Ym4/AAAAAAAAAAI/AAAAAAAAADQ/8KtXErTegzE/photo.jpg?sz=150",
      "track_id": "kYVHxM-IXEE",
      "image": "http://userserve-ak.last.fm/serve/300x300/94242341.png",
      "artist": "Foo Fighters",
      "title": "Best of You",
      "date": "2015-07-31T17:24:03.363Z",
      "like": [],
      "like_count": 0,
      "status": "played",
      "artist_image": "//assets.fanart.tv/fanart/music/67f66c07-6e61-4026-ade5-7e782fad3a5d/artistbackground/foo-fighters-4e3a7e533468e.jpg"
    }
     */

public class Song implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };
    @SerializedName("user_id")
    private String userID;
    @SerializedName("party_id")
    private String partyId;
    @SerializedName("username")
    private String username;
    @SerializedName("userimage")
    private String userImage;
    @SerializedName("track_id")
    private String trackId;
    @SerializedName("image")
    private String trackImage;
    @SerializedName("artist")
    private String artist;
    @SerializedName("artist_image")
    private String artistImage;
    @Expose
    private String title;
    @Expose
    private Date date;
    @SerializedName("like")
    private List<String> listLikes = new ArrayList<>();
    @SerializedName("like_count")
    private int likeCount;
    @SerializedName("status")
    private String status;

    public Song(String userID, String partyId, String username, String userImage, String trackId, String trackImage, String artist, String artistImage, String title, Date date, List<String> listLikes, int likeCount, String status) {
        this.userID = userID;
        this.partyId = partyId;
        this.username = username;
        this.userImage = userImage;
        this.trackId = trackId;
        this.trackImage = trackImage;
        this.artist = artist;
        this.artistImage = artistImage;
        this.title = title;
        this.date = date;
        this.listLikes = listLikes;
        this.likeCount = likeCount;
        this.status = status;
    }

    protected Song(Parcel in) {
        userID = in.readString();
        partyId = in.readString();
        username = in.readString();
        userImage = in.readString();
        trackId = in.readString();
        trackImage = in.readString();
        artist = in.readString();
        artistImage = in.readString();
        title = in.readString();
        long tmpDate = in.readLong();
        date = tmpDate != -1 ? new Date(tmpDate) : null;
        if (in.readByte() == 0x01) {
            listLikes = new ArrayList<>();
            in.readList(listLikes, String.class.getClassLoader());
        } else {
            listLikes = null;
        }
        likeCount = in.readInt();
        status = in.readString();
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", username='" + username + '\'' +
                ", userID='" + userID + '\'' +
                ", partyId='" + partyId + '\'' +
                ", userImage='" + userImage + '\'' +
                ", trackId='" + trackId + '\'' +
                ", trackImage='" + trackImage + '\'' +
                ", artist='" + artist + '\'' +
                ", artistImage='" + artistImage + '\'' +
                ", date=" + date +
                ", listLikes=" + listLikes +
                ", likeCount=" + likeCount +
                ", status='" + status + '\'' +
                '}';
    }

    public String getuserID() {
        return userID;
    }

    public void setuserID(String userID) {
        this.userID = userID;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getTrackImage() {
        return trackImage;
    }

    public void setTrackImage(String trackImage) {
        this.trackImage = trackImage;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtistImage() {
        return artistImage;
    }

    public void setArtistImage(String artistImage) {
        this.artistImage = artistImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public List<String> getListLikes() {
        return listLikes;
    }

    public void setListLikes(List<String> listLikes) {
        this.listLikes = listLikes;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userID);
        dest.writeString(partyId);
        dest.writeString(username);
        dest.writeString(userImage);
        dest.writeString(trackId);
        dest.writeString(trackImage);
        dest.writeString(artist);
        dest.writeString(artistImage);
        dest.writeString(title);
        dest.writeLong(date != null ? date.getTime() : -1L);
        if (listLikes == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(listLikes);
        }
        dest.writeInt(likeCount);
        dest.writeString(status);
    }
}