package com.devchimps.partyapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by MGrim on 10/08/2015.
 */
public class User {
    @SerializedName("user_id")
    private String id;
    private String name;
    @SerializedName("image")
    private String imgURL;

    public User(String id, String name, String imgURL) {
        this.id = id;
        this.name = name;
        this.imgURL = imgURL;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", imgURL='" + imgURL + '\'' +
                '}';
    }
}
