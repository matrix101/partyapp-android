package com.devchimps.partyapp.network;

import android.content.Context;
import android.util.Log;

import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.devchimps.partyapp.storage.Storage;
import com.devchimps.partyapp.util.Constants;

import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Matrix on 04/04/2016.
 */
public class PartyappSocket {
    private static final String TAG = "PartyappSocket";
    private Socket ioSocket;
    private PartyAppProfile partyAppProfile;


    public PartyappSocket(Context context){
        partyAppProfile = Storage.getPartyappProfile(context);
        if (partyAppProfile == null)
            throw new RuntimeException("Partyapp profile cannot be null when creating the socket");
        try {
            Log.i(TAG, "Creating a socket connected to party: " + partyAppProfile.getPartyID());
            ioSocket = IO.socket(Constants.getBaseSocketUrl() + partyAppProfile.getPartyID());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public Socket connect(){
       return ioSocket.connect();
    }

    public Emitter off(){
        return ioSocket.off();
    }

    public Emitter on(String event, Emitter.Listener fn){
        return ioSocket.on(event,fn);
    }

    public boolean connected(){
        return ioSocket.connected();
    }

    public Socket disconnect(){
        return ioSocket.disconnect();
    }
}
