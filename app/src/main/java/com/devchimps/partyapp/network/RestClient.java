package com.devchimps.partyapp.network;

import android.content.Context;
import android.util.Log;

import com.devchimps.partyapp.network.service.ApiServiceInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Matrix on 28/02/2015.
 */
public class RestClient {

    private static final String TAG = "RestClient";

    private static final String BASE_URL = "https://partyapp-cloud.herokuapp.com";
    //private static final String BASE_URL = "http://192.168.1.227:5000/";
    private static final int CONNECTION_TIMEOUT = 15000;
    private static final int HTTP_CACHE_SIZE = 10 * 1024 * 1024;
    private ApiServiceInterface apiService;

    public RestClient(Context context) {

        Gson gson = new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeAdapterFactory())
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        Cache cache = createHttpClientCache(context);
        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    .header("x-partyapp-key", "CqSSPvAYjYY86yNllrD8fe0SMWCR5IO8")
                    .method(original.method(), original.body())
                    .build();
            Response response = chain.proceed(request);

            //retry if something goes wrong
            int tryCount = 0;
            while (!response.isSuccessful() && tryCount < 3) {
                if (response.code() == 400) {//Bad request
                    Log.d(TAG, "Max song added in this playlist");
                    break; //fixme max song added
                }

                Log.d(TAG, "Interceptor: Request is not successful - retrying:" + tryCount + " code: " + response.networkResponse().code());

                tryCount++;

                response = chain.proceed(request);
            }
            return response;
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        httpClient.addInterceptor(logging);

        httpClient.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        httpClient.readTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        httpClient.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS);
        httpClient.cache(cache);
        httpClient.followRedirects(true);
        httpClient.followSslRedirects(true);
        httpClient.retryOnConnectionFailure(true);

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();

        apiService = retrofit.create(ApiServiceInterface.class);
    }

    private static Cache createHttpClientCache(Context context) {
        File cacheDir = context.getDir("service_api_cache", Context.MODE_PRIVATE);
        return new Cache(cacheDir, HTTP_CACHE_SIZE);
    }

    public ApiServiceInterface getApiService() {
        return apiService;
    }
}



