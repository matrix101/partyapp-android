package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matrix on 17/06/2015.
 */
public class InsertSongResponse {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }
}
