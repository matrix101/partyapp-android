package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matrix on 16/06/2015.
 */
public class JoinPartyResponse {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }
}
