package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 21/06/2015.
 */
public class LikeResponse {

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }
}
