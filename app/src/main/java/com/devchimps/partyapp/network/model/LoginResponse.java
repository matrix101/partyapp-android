package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matrix on 16/06/2015.
 */
public class LoginResponse {
    @SerializedName("message")
    private String message;

    @SerializedName("response")
    private PartyAppProfile partyAppProfile;

    public String getMessage() {
        return message;
    }

    public PartyAppProfile getPartyAppProfile() {
        return partyAppProfile;
    }

}
