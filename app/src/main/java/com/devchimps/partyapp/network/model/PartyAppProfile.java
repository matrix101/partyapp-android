package com.devchimps.partyapp.network.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matrix on 14/06/2015.
 */
public class PartyAppProfile implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PartyAppProfile> CREATOR = new Parcelable.Creator<PartyAppProfile>() {
        @Override
        public PartyAppProfile createFromParcel(Parcel in) {
            return new PartyAppProfile(in);
        }

        @Override
        public PartyAppProfile[] newArray(int size) {
            return new PartyAppProfile[size];
        }
    };
    @SerializedName("user_id")
    String idUser;
    @SerializedName("email")
    String email;
    @SerializedName("name")
    String fullName;
    @SerializedName("username")
    String firstName;
    @SerializedName("image")
    String imageURL;
    boolean hasParty;
    String partyID;

    public PartyAppProfile(String idUser, String email, String fullName, String firstName, String imageURL) {
        this.idUser = idUser;
        this.email = email;
        this.fullName = fullName;
        this.firstName = firstName;
        this.imageURL = imageURL;
        this.hasParty = false;
        this.partyID = "";
    }

    protected PartyAppProfile(Parcel in) {
        idUser = in.readString();
        email = in.readString();
        fullName = in.readString();
        firstName = in.readString();
        imageURL = in.readString();
        hasParty = in.readByte() != 0x00;
        partyID = in.readString();
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public boolean hasParty() {
        return hasParty;
    }

    private void setHasParty(boolean hasParty) {
        this.hasParty = hasParty;
    }

    public String getPartyID() {
        return partyID;
    }

    private void setPartyID(String partyID) {
        this.partyID = partyID;
    }

    public void leaveParty() {
        setHasParty(false);
        setPartyID("");
    }

    public void joinParty(String partyID) {
        setHasParty(true);
        setPartyID(partyID);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idUser);
        dest.writeString(email);
        dest.writeString(fullName);
        dest.writeString(firstName);
        dest.writeString(imageURL);
        dest.writeByte((byte) (hasParty ? 0x01 : 0x00));
        dest.writeString(partyID);
    }

    @Override
    public String toString() {
        return "PartyAppProfile{" +
                "idUser='" + idUser + '\'' +
                ", email='" + email + '\'' +
                ", fullName='" + fullName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", imageURL='" + imageURL + '\'' +
                ", hasParty=" + hasParty +
                ", partyID='" + partyID + '\'' +
                '}';
    }
}