package com.devchimps.partyapp.network.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Matrix on 14/06/2015.
 */
public class SocialProfile implements Parcelable {
    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SocialProfile> CREATOR = new Parcelable.Creator<SocialProfile>() {
        @Override
        public SocialProfile createFromParcel(Parcel in) {
            return new SocialProfile(in);
        }

        @Override
        public SocialProfile[] newArray(int size) {
            return new SocialProfile[size];
        }
    };
    String displayName;
    String image;
    String email;

    public SocialProfile(String displayName, String image, String email) {
        this.displayName = displayName;
        this.image = image;
        this.email = email;
    }

    protected SocialProfile(Parcel in) {
        displayName = in.readString();
        image = in.readString();
        email = in.readString();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "SocialProfile{" +
                ", displayName='" + displayName + '\'' +
                ", image='" + image + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(displayName);
        dest.writeString(image);
        dest.writeString(email);
    }
}
