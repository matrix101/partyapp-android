package com.devchimps.partyapp.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SongResult implements Parcelable {

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SongResult> CREATOR = new Parcelable.Creator<SongResult>() {
        @Override
        public SongResult createFromParcel(Parcel in) {
            return new SongResult(in);
        }

        @Override
        public SongResult[] newArray(int size) {
            return new SongResult[size];
        }
    };
    @SerializedName("title")
    private String title;
    @SerializedName("artist")
    private String artist;
    @SerializedName("image")
    private String imgURL;
    @SerializedName("track_id")
    private String track_id;

    public SongResult(String title, String artist, String imgURL, String track_id) {
        this.title = title;
        this.artist = artist;
        this.imgURL = imgURL;
        this.track_id = track_id;
    }

    protected SongResult(Parcel in) {
        title = in.readString();
        artist = in.readString();
        imgURL = in.readString();
        track_id = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public String getTrackID() {
        return track_id;
    }

    public void setTrackID(String track_id) {
        this.track_id = track_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(artist);
        dest.writeString(imgURL);
        dest.writeString(track_id);
    }
}