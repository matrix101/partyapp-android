package com.devchimps.partyapp.network.model;

import com.devchimps.partyapp.model.Party;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MGrim on 07/08/2015.
 */
public class UserPartiesResponse {
    @SerializedName("response")
    List<Party> parties;
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public List<Party> getUserParties() {
        return parties;
    }
}
