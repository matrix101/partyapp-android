package com.devchimps.partyapp.network.model;

import com.devchimps.partyapp.model.Song;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by MGrim on 07/08/2015.
 */
public class UserPlaylistResponse {
    @SerializedName("response")
    List<Song> playlist;
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public List<Song> getUserPlaylist() {
        return playlist;
    }
}
