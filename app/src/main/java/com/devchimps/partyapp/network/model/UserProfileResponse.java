package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Francesco on 04/07/2015.
 */
public class UserProfileResponse {
    @SerializedName("response")
    UserStats userStats;
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public UserStats getUserStats() {
        return userStats;
    }
}
