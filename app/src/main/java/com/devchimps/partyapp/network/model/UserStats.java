package com.devchimps.partyapp.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Matrix on 05/07/2015.
 */
public class UserStats {

    @SerializedName("profile")
    PartyAppProfile profile;

    @SerializedName("parties")
    int parties;

    @SerializedName("songs")
    int songs;


    public PartyAppProfile getProfile() {
        return profile;
    }

    public int getTotalParties() {
        return parties;
    }

    public int getTotalSongs() {
        return songs;
    }
}
