package com.devchimps.partyapp.network.service;

import com.devchimps.partyapp.network.model.Coordinate;
import com.devchimps.partyapp.network.model.InsertSongResponse;
import com.devchimps.partyapp.network.model.JoinPartyResponse;
import com.devchimps.partyapp.network.model.LikeResponse;
import com.devchimps.partyapp.network.model.LoginResponse;
import com.devchimps.partyapp.network.model.SocialProfile;
import com.devchimps.partyapp.network.model.SongResult;
import com.devchimps.partyapp.network.model.UserPartiesResponse;
import com.devchimps.partyapp.network.model.UserPlaylistResponse;
import com.devchimps.partyapp.network.model.UserProfileResponse;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiServiceInterface {
    //LOGIN

    @POST("/api/auth/google")
    Observable<LoginResponse> loginGoogle(@Body SocialProfile profile);

    @POST("/api/auth/facebook")
    Observable<LoginResponse> loginFacebook(@Body SocialProfile profile);

    @GET("/api/search")
    Observable<List<SongResult>> getSongs(@Query("q") String songName, @Query("user_id") String userID);

    @FormUrlEncoded
    @POST("/api/party/join")
    Observable<JoinPartyResponse> joinParty(@Field("party_id") String partyID, @Field("user_id") String userID);

    @FormUrlEncoded
    @POST("/api/playlist")
    Observable<InsertSongResponse> insertSong(@Field("artist") String artist, @Field("track_id") String trackID, @Field("image") String image, @Field("title") String title, @Field("party_id") String partyID, @Field("user_id") String userID);

    @FormUrlEncoded
    @POST("/api/playlist/like")
    Observable<LikeResponse> likeSong(@Field("track_id") String youtubeID, @Field("party_id") String partyID, @Field("user_id") String userID);

    //USER
    @GET("/api/user/{user_id}")
    Observable<UserProfileResponse> getUserProfile(@Path("user_id") String userID);

    @GET("/api/user/{user_id}/playlist")
    Observable<UserPlaylistResponse> getUserPlaylist(@Path("user_id") String userID, @Query("skip") int skip, @Query("limit") int limit);

    @GET("/api/user/{user_id}/party")
    Observable<UserPartiesResponse> getUserParties(@Path("user_id") String userID);

    @GET("/api/party/find/near")
    Observable<UserPartiesResponse> getPartiesNearLocation(@Query(value = "ll", encoded = true) Coordinate coordinates); //add userID?

    @GET("/api/party/find/name")
    Observable<UserPartiesResponse> getPartyByName(@Query("name") String name); //add userID?

    //TODO
    //getUsersInParty
    //getPlayedSongs
    //getSongDetails
}
