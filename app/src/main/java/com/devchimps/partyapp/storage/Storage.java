package com.devchimps.partyapp.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.devchimps.partyapp.network.model.PartyAppProfile;
import com.google.gson.Gson;

/**
 * Created by Matrix on 04/04/2016.
 */
public class Storage {
    private static final String TAG = Storage.class.getSimpleName();
    private static PartyAppProfile partyappProfile;


    public static void savePartyAppProfile(Context context, PartyAppProfile partyAppProfile) {
        Log.i(TAG, "storing partyAppProfile");
        SharedPreferences preferences = context.getSharedPreferences("com.devchimps.partyapp", Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(partyAppProfile);
        preferencesEditor.putString("partyAppProfile", json);
        preferencesEditor.apply();
    }

    public static PartyAppProfile getPartyappProfile(Context context) {
        if (partyappProfile != null)
            return partyappProfile;
        else
            return loadPartyAppProfile(context);
    }

    private static PartyAppProfile loadPartyAppProfile(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("com.devchimps.partyapp", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = preferences.getString("partyAppProfile", "");
        partyappProfile = gson.fromJson(json, PartyAppProfile.class);
        if (partyappProfile != null)
            return partyappProfile;
        else {
            Log.e(TAG, "loadPartyAppProfile: profile is null");
            return null;
        }
    }

    public static void deletePartyAppProfile(Context context) {
        partyappProfile = null;
        SharedPreferences preferences = context.getSharedPreferences("com.devchimps.partyapp", Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();
        preferencesEditor.remove("partyAppProfile");
        preferencesEditor.apply();
    }
}
