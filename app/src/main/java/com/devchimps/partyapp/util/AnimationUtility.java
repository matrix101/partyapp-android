package com.devchimps.partyapp.util;

import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.os.Handler;
import android.widget.TextView;

/**
 * Created by Matrix on 05/07/2015.
 */
public class AnimationUtility {
    public static void animationCountTo(int from, int to, final TextView view) {
        final ValueAnimator animator = new ValueAnimator();
        animator.setObjectValues(from, to);
        animator.addUpdateListener(animation -> view.setText(String.valueOf(animation.getAnimatedValue())));
        animator.setEvaluator(new TypeEvaluator<Integer>() {
            public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
                return Math.round(startValue + (endValue - startValue) * fraction);
            }
        });
        animator.setDuration(2000);
        Handler handler = new Handler();
        handler.postDelayed(animator::start, 1000);
    }
}
