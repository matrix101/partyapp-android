package com.devchimps.partyapp.util;

/**
 * Created by MGrim on 27/08/2015.
 */
public class Constants {

    public static String getMapBoxImgUrl(float lat, float lng, int width, int height) {
        return "http://api.mapbox.com/v4/devchimps.afc56c38/pin-l+ef6c00(" + lat + "," + lng + ")/" + lat + "," + lng + ",15/" + width + "x" + height + ".png?access_token=pk.eyJ1IjoiZGV2Y2hpbXBzIiwiYSI6IjA1NDg3OTBjNjQ5Mjc1OWMwOTM0YjRiNmVmYzdmNjc4In0.LpC1wNQOZBSl_kwgntRlhA";
    }

    public static String getBaseSocketUrl() {
        return "http://partyapp-cloud.herokuapp.com/";
    }

    // TODO: 26/11/2015 getGoogleMapsUrl
}
