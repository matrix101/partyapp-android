package com.devchimps.partyapp.util;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by MGrim on 21/08/2015.
 */
public class SnackbarFactory {

    public static Snackbar make(View view, String message, int color, int duration) {
        Snackbar snackbar = Snackbar.make(view, message, duration);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(view.getContext(), color));

        return snackbar;
    }

    public static Snackbar make(View view, int message, int color, int duration) {
        Snackbar snackbar = Snackbar.make(view, message, duration);
        ViewGroup group = (ViewGroup) snackbar.getView();
        group.setBackgroundColor(ContextCompat.getColor(view.getContext(), color));
        return snackbar;
    }
}
