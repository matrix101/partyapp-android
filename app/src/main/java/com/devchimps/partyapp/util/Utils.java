package com.devchimps.partyapp.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.view.inputmethod.InputMethodManager;

import com.devchimps.partyapp.model.Song;

/**
 * Created by Matrix on 19/01/2015.
 */

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();


    public static boolean isNetworkOn(Context context) {
        ConnectivityManager connMgr =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static boolean isAirplaneModeOn(Context context) {

        return Settings.System.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;

    }

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }



    /*
        // TODO: 26/11/2015 remove this method
        public static void getSHA(Context context) {
            try {
                PackageInfo info = context.getPackageManager().getPackageInfo("com.devchimps.partyapp", PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.i("HASH KEY:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {
            } catch (NoSuchAlgorithmException e) {
            }
        }
    */
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Song checkUrls(Song currentTrack) {
        if (!currentTrack.getUserImage().startsWith("http"))
            currentTrack.setUserImage("http:" + currentTrack.getUserImage());
        if (!currentTrack.getArtistImage().startsWith("http"))
            currentTrack.setArtistImage("http:" + currentTrack.getArtistImage());
        if (!currentTrack.getTrackImage().startsWith("http"))
            currentTrack.setTrackImage("http:" + currentTrack.getTrackImage());
        return currentTrack;
    }
}
